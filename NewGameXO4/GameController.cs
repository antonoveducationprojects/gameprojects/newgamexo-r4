﻿/*
 * Created by SharpDevelop.
 * User: guyver
 * Date: 27.11.2018
 * Time: 8:29
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace NewGameXO
{
	/// <summary>
	/// Description of ......
	/// </summary>
	/// 
	public enum GameState
	{
		inProgress,
		Winner,
		GameOver
	}
	public class GameController
	{
		int[,] GameMap= new int[3,3];
		public int currentStep {get;private set;}
		public GameState gameState {get;private set;}
		protected const int codeX=1,codeO=0,codeFree=-1;
		public int WinnerCode {get;private set;}
		protected Graphics gDev;
		protected GameFild gameFild;
		protected AbstractGameFigure[] figures=new AbstractGameFigure[2];
		public GameController(Graphics dev)
		{
			gameState=GameState.inProgress;
			gDev=dev;
			currentStep=0;
			gameFild=new GameFild();
			figures[0]=new SimpleOFigure();
			figures[1]=new SimpleXFigure();
			ResetMap();
		}
		protected void ResetMap()
		{
			for(int i=0;i<3;++i)
				for(int j=0;j<3;++j)
					GameMap[i,j]=codeFree;
		}
		public void DisplayMap(Graphics gDev)
		{
			int figureCode;
			gameFild.Display(gDev);
			for(int i=0;i<3;++i)
				for(int j=0;j<3;++j)
			    {
					figureCode=GameMap[i,j];				
					if(figureCode==codeFree)
						continue;
					figures[figureCode].Display(gDev,i*200,j*200);
				}
		}
		public void MakeJob(int x, int y)
		{
			if(gameState==GameState.GameOver)
				return;
			int i=x/200;
			int j=y/200;
			if(GameMap[i,j]!=codeFree)
				return;
			GameMap[i,j]=currentStep%2;
			currentStep++;
		}
		public bool CheckWinner()
		{
			int[] checkRow,checkCol,checkDiag,checkDiag2;
			checkRow=new int[2];
			checkCol=new int[2];
			checkDiag=new int[2];
			checkDiag2=new int[2];
			for(int i=0;i<3;++i)
			{
				checkRow[0]=checkRow[1]=checkCol[0]=checkCol[1]=0;
				for(int j=0;j<3;++j)
				{
					if(-1!=GameMap[i,j])
						checkRow[GameMap[i,j]]++;
					if(-1!=GameMap[j,i])
						checkCol[GameMap[j,i]]++;
				}
				for(int j=0;j<2;++j)
				{
					if(3==checkRow[j]||3==checkCol[j])
					{
						gameState=GameState.Winner;
						WinnerCode=j;
						return true;
					}
				}
				if(-1!=GameMap[i,i])
					checkDiag[GameMap[i,i]]++;
				if(-1!=GameMap[2-i,i])
					checkDiag2[GameMap[2-i,i]]++;
			}
			for(int j=0;j<2;++j)
			{
				if(3==checkDiag[j]||3==checkDiag2[j])
				{
					gameState=GameState.Winner;
					WinnerCode=j;
					return true;
				}
			}
			if(9==currentStep)
			{
				gameState=GameState.GameOver;
				return true;
			}
			return false;		
		}
		public void ResetGame()
		{
			ResetMap();
			WinnerCode=-1;
			gameState=GameState.inProgress;
			currentStep=0;
		}
	}
}
