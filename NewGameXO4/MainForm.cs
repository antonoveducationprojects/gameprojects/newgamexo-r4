﻿/*
 * Created by SharpDevelop.
 * User: Yuriy
 * Date: 26.11.2018
 * Time: 18:18
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace NewGameXO
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		Pen myPen = new Pen(Color.Black);
		SolidBrush bgBrush=new SolidBrush(Color.Green);
		SolidBrush pixelBrush=new SolidBrush(Color.Aqua);
		Int32 rectWidth=200;
		protected Graphics gDev;
		protected GameController game;
		public MainForm()
		{
			InitializeComponent();
			gDev=Graphics.FromHwnd(paintArea.Handle);
			game=new GameController(gDev);
		}
		void PaintAreaPaint(object sender, PaintEventArgs e)
		{
			Graphics myDev=e.Graphics;
			game.DisplayMap(myDev);
		}
		void PaintAreaMouseClick(object sender, MouseEventArgs e)
		{
			if(game.gameState!=GameState.inProgress)
				return;
			int x=e.Location.X;
			int y=e.Location.Y;
			game.MakeJob(x,y);
			stepLabel.Text=game.currentStep.ToString();
			bool res=game.CheckWinner();
			if(res)
			{
				if(game.gameState==GameState.GameOver)
					msgLabel.Text="GAME OVER!";
				if(game.gameState==GameState.Winner)
					msgLabel.Text=string.Format("{0} is Winn!!!",game.WinnerCode==0?"O":"X");
			}
			paintArea.Refresh();
		}
		void ResetButtonClick(object sender, EventArgs e)
		{
			game.ResetGame();
			msgLabel.Text="in Process!!!";
			stepLabel.Text=game.currentStep.ToString();
			paintArea.Refresh();
		}

	}
}
