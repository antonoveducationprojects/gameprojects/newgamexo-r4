﻿/*
 * Created by SharpDevelop.
 * User: Yuriy
 * Date: 26.11.2018
 * Time: 18:18
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace NewGameXO
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.PictureBox paintArea;
		private System.Windows.Forms.Button ResetButton;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label stepLabel;
		private System.Windows.Forms.Label msgLabel;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.paintArea = new System.Windows.Forms.PictureBox();
			this.ResetButton = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.stepLabel = new System.Windows.Forms.Label();
			this.msgLabel = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.paintArea)).BeginInit();
			this.SuspendLayout();
			// 
			// paintArea
			// 
			this.paintArea.Location = new System.Drawing.Point(12, 7);
			this.paintArea.MaximumSize = new System.Drawing.Size(600, 600);
			this.paintArea.MinimumSize = new System.Drawing.Size(600, 600);
			this.paintArea.Name = "paintArea";
			this.paintArea.Size = new System.Drawing.Size(600, 600);
			this.paintArea.TabIndex = 0;
			this.paintArea.TabStop = false;
			this.paintArea.Paint += new System.Windows.Forms.PaintEventHandler(this.PaintAreaPaint);
			this.paintArea.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PaintAreaMouseClick);
			// 
			// ResetButton
			// 
			this.ResetButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.ResetButton.Location = new System.Drawing.Point(495, 614);
			this.ResetButton.Name = "ResetButton";
			this.ResetButton.Size = new System.Drawing.Size(115, 35);
			this.ResetButton.TabIndex = 1;
			this.ResetButton.Text = "RESET";
			this.ResetButton.UseVisualStyleBackColor = true;
			this.ResetButton.Click += new System.EventHandler(this.ResetButtonClick);
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1.Location = new System.Drawing.Point(7, 613);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(134, 26);
			this.label1.TabIndex = 2;
			this.label1.Text = "Current step:";
			// 
			// stepLabel
			// 
			this.stepLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.stepLabel.Location = new System.Drawing.Point(137, 616);
			this.stepLabel.Name = "stepLabel";
			this.stepLabel.Size = new System.Drawing.Size(24, 23);
			this.stepLabel.TabIndex = 3;
			this.stepLabel.Text = "0";
			// 
			// msgLabel
			// 
			this.msgLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.msgLabel.Location = new System.Drawing.Point(188, 613);
			this.msgLabel.Name = "msgLabel";
			this.msgLabel.Size = new System.Drawing.Size(301, 36);
			this.msgLabel.TabIndex = 4;
			this.msgLabel.Text = "in Process";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(624, 661);
			this.Controls.Add(this.msgLabel);
			this.Controls.Add(this.stepLabel);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.ResetButton);
			this.Controls.Add(this.paintArea);
			this.MaximumSize = new System.Drawing.Size(640, 700);
			this.MinimumSize = new System.Drawing.Size(640, 700);
			this.Name = "MainForm";
			this.Text = "NewGameXO";
			((System.ComponentModel.ISupportInitialize)(this.paintArea)).EndInit();
			this.ResumeLayout(false);

		}

		}
	}